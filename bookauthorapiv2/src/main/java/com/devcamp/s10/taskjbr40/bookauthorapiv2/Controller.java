package com.devcamp.s10.taskjbr40.bookauthorapiv2;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
    @CrossOrigin
    @GetMapping("/books")
        public ArrayList<Book> getBook(){
            ArrayList<Author> authorlist1 = new ArrayList<>();
            ArrayList<Author> authorlist2 = new ArrayList<>();
            ArrayList<Author> authorlist3 = new ArrayList<>();
            Author author1 = new Author("Tom Cruise", "tomcruise@gmail.com", 'm');
            Author author2 = new Author("Linh Giang", "linhgiang@gmail.com", 'f');
            Author author3 = new Author("Văn Ninh", "vanninh@gmail.com", 'm');
            Author author4 = new Author("Đức Thạnh", "ducthanh@gmail.com", 'm');
            Author author5 = new Author("Hoàng Hải", "hoanghai@gmail.com", 'f');
            Author author6 = new Author("Hồng Hoa", "honghoa@gmail.com", 'f');
            System.out.println(author1.toString());
            System.out.println(author2.toString());
            System.out.println(author3.toString());
            System.out.println(author4.toString());
            System.out.println(author5.toString());
            System.out.println(author6.toString());
            authorlist1.add(author1);
            authorlist1.add(author2);
            authorlist2.add(author3);
            authorlist2.add(author4);
            authorlist3.add(author5);
            authorlist3.add(author6);
            Book book1 = new Book("Ngày mới", authorlist1, 10000, 2);
            Book book2 = new Book("Ngày cũ", authorlist2, 20000, 2);
            Book book3 = new Book("Ngày tương lai", authorlist3, 10000, 2);
            System.out.println(book1.toString());
            System.out.println(book2.toString());
            System.out.println(book3.toString());
            ArrayList<Book> books = new ArrayList<>();
            books.add(book1);
            books.add(book2);
            books.add(book3);
            return books;
        }
}
