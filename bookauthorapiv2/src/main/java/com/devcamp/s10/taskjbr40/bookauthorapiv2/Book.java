package com.devcamp.s10.taskjbr40.bookauthorapiv2;

import java.util.ArrayList;

public class Book {
    String name;
    ArrayList<Author> authors;
    double price;
    int qty =0;
    
    public Book(String name, ArrayList<Author> authors, double price) {
        this.name = name;
        this.authors = authors;
        this.price = price;
    }

    public Book(String name, ArrayList<Author> authors, double price, int qty) {
        this.name = name;
        this.authors = authors;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return name;
    }

    public ArrayList<Author> getAuthors() {
        return authors;
    }

    public double getPrice() {
        return price;
    }

    public int getQty() {
        return qty;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }
    public String getAuthorNames() {
        String authorName ="";
        for (int i =0; i<authors.size(); i++){
            authorName += authors.get(i).getName() + " ";      
        }
        return authorName;
    }

    @Override
    public String toString() {
        return "Book [authors=" + authors + ", name=" + name + ", price=" + price + ", qty=" + qty + "]";
    }

    
    
}
